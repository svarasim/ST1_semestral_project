package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class BaseTest {
    protected WebDriver driver;

    protected String email = "simonsvara@gmail.com";
    protected String password = "VýpečkySeZelim123";

    @BeforeClass
    @Parameters({"driverPath", "baseUrl"})
    public void setUp() {
        // WebDriver path
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
    }

    @BeforeMethod
    public void beforeMethod() {
        // Inicializace WebDriver
        driver = new ChromeDriver();
        driver.manage().window().maximize();;
        driver.get("https://www.surveyhero.com/");

    }

    @AfterClass
    public void tearDown() {
        // Close browser
        if (driver != null) {
            driver.quit();
        }
    }
}

package tests;

import base.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;

public class LoginTest extends BaseTest {

    @Test
    public void testValidLogin() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(email, password);


        boolean isEmailDisplayed = loginPage.isLoginSuccessful(email);
        Assert.assertTrue(isEmailDisplayed, "Profile panel with user email was not displayed");

        driver.quit();
    }

    @Test
    public void testInvalidLogin(){

        String email = "simonsvara@gmail.com";
        String password = "VýpečkySeZelim1234";


        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(email,password);

        boolean isErrorDisplayed = loginPage.isLoginErrorDisplayed();
        Assert.assertTrue(isErrorDisplayed, "The login error message was not displayed or was incorrect.");


        driver.quit();
    }
}

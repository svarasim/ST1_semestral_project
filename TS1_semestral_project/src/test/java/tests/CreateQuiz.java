package tests;

import base.BaseTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MainPage;

public class CreateQuiz extends BaseTest {
    // Initialize LoginPage
    LoginPage loginPage;
    MainPage mainPage;

    @Test
    public void CreateNewQuiz(){
        loginPage = new LoginPage(driver);
        mainPage = new MainPage(driver);

        loginPage.login(email, password);
        // Waiting for page load
        loginPage.isLoginSuccessful(email);

        mainPage.createNewQuiz();
        mainPage.startFromScratch();
        mainPage.addTittleAndName("ahoj","ahoj");
    }
}

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SeleniumTest {

    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");

        driver = new ChromeDriver();
    }

    @Test
    public void testPageTitle() throws InterruptedException {
        driver.get("https://docs.google.com/forms/u/0/?tgif=d&ec=asw-forms-hero-goto");

        String title = driver.getTitle();
        System.out.println("Title of the page is: " + title);


        Thread.sleep(5000);

    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}

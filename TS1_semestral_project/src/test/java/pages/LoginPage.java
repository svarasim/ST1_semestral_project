package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class LoginPage {
    private WebDriver driver;
    private WebDriverWait wait;

    // Definování webových prvků pomocí @FindBy
    @FindBy(css = "#login-menu a.usr-button.trans")
    private WebElement signInButton;

    @FindBy(css = ".usr-form-item input[type='email']")
    private WebElement emailInputField;

    @FindBy(css = "#password")
    private WebElement passwordInputField;

    @FindBy(css = "input[type='submit']")
    private WebElement signInSubmitButton;

    @FindBy(css = "#customer-information .email-address")
    private WebElement emailDisplayed;

    @FindBy(css = ".usr-notify.error")
    private WebElement loginError;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10)); // Set explicit waiting
        // Inicializace prvků pomocí PageFactory
        PageFactory.initElements(driver, this);
    }

    public void login(String email, String password){
        // Click on the 'Sign In' button in the header
        wait.until(ExpectedConditions.visibilityOf(signInButton)).click();

        // Wait until the email input field is visible
        wait.until(ExpectedConditions.visibilityOf(emailInputField)).sendKeys(email);

        // Enter the password
        wait.until(ExpectedConditions.visibilityOf(passwordInputField)).sendKeys(password);

        // Submit the login form
        wait.until(ExpectedConditions.visibilityOf(signInSubmitButton)).click();

    }

    // Method to verify the displayed email after login
    public boolean isLoginSuccessful(String expectedEmail) {
        // Wait for the element to be visible
        WebElement emailElement = wait.until(ExpectedConditions.visibilityOf(emailDisplayed));

        // Verify that the email displayed is the one expected
        return emailElement.getText().equals(expectedEmail);
    }

    // New method to check for login error message
    public boolean isLoginErrorDisplayed() {
        // Wait for the error message element to be visible
        WebElement errorElement = wait.until(ExpectedConditions.visibilityOf(loginError));

        // Check if the error message is correct
        String expectedErrorMessage = "Your credentials are not correct.";
        return errorElement.getText().contains(expectedErrorMessage);
    }
}
package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class MainPage {
    private WebDriver driver;
    private WebDriverWait wait;


    // Declare Web element
    @FindBy(css = "div.usr-box.sv-no-surveys-message a.usr-button.green.large")
    private WebElement createNewSurveyButtonFirst;
    @FindBy(css = "div.header div.menu a.usr-button.small.green")
    private WebElement createNewSurveyButtonNonFirst;
    @FindBy(css = "div#new-survey-selection a:first-of-type")
    private WebElement startFromScratchButton;


    @FindBy(id = "ol-body")
    private WebElement modalBody;

    @FindBy(id = "ol-main-body")
    private WebElement modalMainBody;

    @FindBy(css = "input[name='survey-title']")
    private WebElement surveyTitleInput;

    @FindBy(css = "input[name='internal-name']")
    private WebElement internalNameInput;


    @FindBy(css = "button.usr-button.green.medium[type='submit']")
    private WebElement createNewSurveySubmitButton;


    public MainPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10)); // Set explicit waiting
        PageFactory.initElements(driver, this);
    }


    // Click on Create new survey
    public void createNewQuiz() {

        int retries = 2; // Počet pokusů

        for (int i = 0; i < retries; i++) {
            try {
                if (i == 0) {

                   createNewSurveyButtonFirst.click();
                } else {
                    createNewSurveyButtonNonFirst.click();
                }
                return;
            } catch (Exception e) {
                // Pokud tlačítko není nalezeno, počkáme a zkusíme to znovu
                try {
                    Thread.sleep(1000); // Počkáme 2 sekundy před dalším pokusem
                } catch (InterruptedException ie) {
                    Thread.currentThread().interrupt();
                }
            }
        }
        throw new RuntimeException("Failed to click 'Create New Survey' button after multiple attempts");
    }

    // Click on Start from scratch
    public void startFromScratch() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ol-body")));
        ((JavascriptExecutor) driver).executeScript("window.location.href='" + "/user/surveys/new/scratch" + "'");
    }

    public void addTittleAndName(String tittle, String internalName){
        // Wait for the main body to be visible
        wait.until(ExpectedConditions.visibilityOf(modalMainBody));

        surveyTitleInput.sendKeys(tittle);
        internalNameInput.sendKeys(internalName);
        createNewSurveySubmitButton.click();
    }

}
